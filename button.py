# -*- coding: utf-8 -*-

import pygame, image_library
from pygame import *

class Button(sprite.Sprite):
    def __init__(self, x, y, text):
        sprite.Sprite.__init__(self)
        self.image = image_library.get_image('assets/img/button.png')
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.image = pygame.transform.scale(self.image, (self.width * 2, self.height * 2))
        self.x = x
        self.y = y
        self.text = text
        self.rect = Rect(x, y, self.width * 2, self.height * 2)

    def draw(self, screen):
        screen.blit(self.image, (self.rect.x, self.rect.y))

