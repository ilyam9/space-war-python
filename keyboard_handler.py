import pygame
from pygame import *

def handle_keyboard(e, l, r, u, d):
	fired = False
	spawn_bullet = False
	if e.type == KEYDOWN and e.key == K_LEFT:
		l = True
	if e.type == KEYDOWN and e.key == K_RIGHT:
		r = True
	if e.type == KEYDOWN and e.key == K_UP:
		u = True
	if e.type == KEYDOWN and e.key == K_DOWN:
		d = True
	if e.type == KEYDOWN and e.key == K_z and fired == False:
		spawn_bullet = True
		fired = True
	if e.type == KEYUP and e.key == K_LEFT:
		l = False
	if e.type == KEYUP and e.key == K_RIGHT:
		r = False
	if e.type == KEYUP and e.key == K_UP:
		u = False
	if e.type == KEYUP and e.key == K_DOWN:
		d = False
	if e.type == KEYUP and e.key == K_z:
		fired = False

	return(l, r, u, d, spawn_bullet)

def handle_menu_mouse(e, pos, button, button_rect):
	if e.type == MOUSEBUTTONDOWN and button_rect.collidepoint(pos[0], pos[1]) and e.button == 1:
		return True

def handle_dialogue_input(e):
	c = False
	pressed_z = False
	if e.type == KEYDOWN and pressed_z == False and e.key == K_z:
		c = True
		pressed_z = True
	if e.type == KEYUP and e.key == K_z:
		pressed_z = False
	return c


