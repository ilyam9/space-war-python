# -*- coding: utf-8 -*-

import pygame
from pygame import *

import math, image_library, enemy_bullet, random

MOVE_SPEED = 4

class Enemy2(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.velX = 0   
        self.velY = 0
        self.x = x 
        self.y = y
        self.health = 10
        self.image = image_library.get_image('assets/img/enemy2.png')
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.image = pygame.transform.scale(self.image, (self.width * 2, self.height * 2))
        self.rect = Rect(x, y, self.width * 2, self.height * 2)
        self.dead = False
        self.timer = 0
        self.hit = False
        self.shot = False
        self.shoot_x = random.randint(0, 800)

    def update(self):
        if not self.dead:
            self.velY = 0
            self.velX = -MOVE_SPEED
            self.rect.x += self.velX
            self.rect.y += self.velY
            if self.health <= 0:
                self.dead = True 
            if self.dead:
                self.kill()
        
    def draw(self, screen): 
        if not self.dead:
            screen.blit(self.image, (self.rect.x, self.rect.y))