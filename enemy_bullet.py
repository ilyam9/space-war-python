# -*- coding: utf-8 -*-

import pygame, image_library, math
from pygame import *

class EnemyBullet(sprite.Sprite):
    def __init__(self, x, y, direction):
        sprite.Sprite.__init__(self)
        self.vel = 6 
        self.x = x 
        self.y = y
        self.direction = direction
        self.damage = 10
        self.image = image_library.get_image('assets/img/enemy_bullet.png')
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.image = pygame.transform.scale(self.image, (self.width * 2, self.height * 2))
        self.rect = Rect(x, y, self.width * 2, self.height * 2)

    def update(self):
        if self.direction == 1:
            self.rect.x += self.vel
        if self.direction == 2:
            self.rect.x -= self.vel
        if self.direction == 3:
            self.rect.y += self.vel
        if self.direction == 4:
            self.rect.y -= self.vel
    def draw(self, screen):
        screen.blit(self.image, (self.rect.x, self.rect.y))