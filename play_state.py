# -*- coding: utf-8 -*-

import pygame, sys, image_library, random
from pygame import *

from player import *
from bullet import *
from enemy import *
from enemy2 import *
from background import *
from keyboard_handler import *
from collision_detection import *
from wave import *
from dialogues import *
from enemy_bullet import *

def play(screen, fpsClock):
    player = Player(0, 0)
    l = r = u = d = False
    pause_font = pygame.font.Font("assets/fonts/ARCADECLASSIC.ttf", 30)
    pause_text = pause_font.render('PAUSED', 1, (255,255,255))

    bullets = sprite.Group()
    enemies = sprite.Group()
    enemy_bullets = sprite.Group()

    background = Background(0, 0)
    background2 = Background(800, 0)

    pause = True

    enemies.add(spawn_wave("assets/txt/wave.txt"))
    spawn_bullet = False
    dialogue = True
    dialogue_button = False
    current_phrase = 0
    current_dialogue = load_dialogue("assets/txt/dialogues.txt")
    
    while True:
        for e in pygame.event.get(): 
            if e.type == QUIT:
                exit()   
            if e.type == KEYDOWN and e.key == K_ESCAPE:
                if pause == True and dialogue == False:
                    pause = False
                else:
                    pause = True
            l, r, u, d, spawn_bullet = handle_keyboard(e, l, r, u, d)
            if dialogue:
                dialogue_button = handle_dialogue_input(e)

        if not pause:
            player.update(l, r, u, d)
            background.update()
            background2.update()
            for enemy in enemies.sprites():
                if not enemy.dead:
                    enemy.update()
                    collide_player(enemy, player)
                if enemy.shot == False:
                    if enemy.shoot_x - 4 <= enemy.rect.x <= enemy.shoot_x + 4:
                        enemy_bullets.add(EnemyBullet(enemy.rect.x, enemy.rect.y, 1))
                        enemy_bullets.add(EnemyBullet(enemy.rect.x, enemy.rect.y, 2))
                        enemy_bullets.add(EnemyBullet(enemy.rect.x, enemy.rect.y, 3))
                        enemy_bullets.add(EnemyBullet(enemy.rect.x, enemy.rect.y, 4))
                        enemy.shot = True

            if spawn_bullet and not player.dead:
                bullet = Bullet(player.rect.x + player.width + 6, player.rect.y + player.height / 2 + 6)
                bullets.add(bullet)
                spawn_bullet = False
            for bullet in bullets.sprites():
                bullet.update()
                for enemy in enemies.sprites():
                    collide_bullets(bullet, enemy)
            for enemy_bullet in enemy_bullets.sprites():
                enemy_bullet.update()
                collide_player(enemy_bullet, player)

        screen.fill((0,0,0))
        background.draw(screen)
        background2.draw(screen)
        player.draw(screen)
        for enemy in enemies.sprites():
            if not enemy.dead:
                enemy.draw(screen)

        for bullet in bullets:
            bullet.draw(screen)
        for enemy_bullet in enemy_bullets:
            enemy_bullet.draw(screen)

        if dialogue:
            draw_dialogue(screen, current_dialogue, current_phrase)
            if dialogue_button:
                dialogue_button = False
                if current_phrase < len(current_dialogue[2]) - 1:
                    current_phrase += 1
                else:
                    dialogue = False
                    pause = False
        if not dialogue and pause:
            screen.blit(pause_text, (345, 260))

        pygame.display.update() 

        fpsClock.tick(60)