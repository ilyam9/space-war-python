# -*- coding: utf-8 -*-

import pygame
from pygame import *

from play_state import *
from menu_state import *

WIN_WIDTH = 800
WIN_HEIGHT = 600

def main():
    pygame.init()
    fpsClock = pygame.time.Clock()
    screen = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT))
    pygame.display.set_caption("Test Game")
    menu(screen, fpsClock)

main()