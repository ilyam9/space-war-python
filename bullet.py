# -*- coding: utf-8 -*-

import pygame, image_library
from pygame import *

class Bullet(sprite.Sprite):
	def __init__(self, x, y):
		sprite.Sprite.__init__(self)
		self.velX = 10   
		self.x = x 
		self.y = y
		self.damage = 10
		self.image = image_library.get_image('assets/img/bullet.png')
		self.width = self.image.get_width()
		self.height = self.image.get_height()
		self.image = pygame.transform.scale(self.image, (self.width * 2, self.height * 2))
		self.rect = Rect(x, y, self.width * 2, self.height * 2)

	def update(self):
	  self.rect.x += self.velX

	def draw(self, screen):
	  screen.blit(self.image, (self.rect.x, self.rect.y))