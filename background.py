# -*- coding: utf-8 -*-

import pygame
from pygame import *

import math, image_library

class Background(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.velX = 0   
        self.velY = 0
        self.x = x
        self.y = y
        self.image = image_library.get_image('assets/img/background.png')
        self.width = self.image.get_width()
        self.height = self.image.get_height()

    def update(self):
        self.x -= 1
        if self.x < -800:
            self.x = 800
        
    def draw(self, screen): 
        screen.blit(self.image, (self.x, self.y))