# -*- coding: utf-8 -*-

import pygame, image_library
from pygame import *

MOVE_SPEED = 6

class Player(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.velX = 0   
        self.velY = 0
        self.x = x 
        self.y = y
        self.image = image_library.get_image('assets/img/player_ship.png')
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.image = pygame.transform.scale(self.image, (self.width * 2, self.height * 2))
        self.rect = Rect(x, y, self.width * 2, self.height * 2)
        self.dead = False

    def update(self, l, r, u, d):
        if not self.dead:
            if l:
                self.velX = -MOVE_SPEED
            if r:
                self.velX = MOVE_SPEED
            if u:
                self.velY = -MOVE_SPEED
            if d:
                self.velY = MOVE_SPEED
            if not (l or r):
                self.velX = 0
            if not (u or d):
                self.velY = 0

            self.rect.x += self.velX
            self.rect.y += self.velY

            if self.rect.x < 0:
                self.rect.x = 0;
            if self.rect.x > 800 - self.width - 18:
                self.rect.x = 800 - self.width - 18
            if self.rect.y < 0:
                self.rect.y = 0;
            if self.rect.y > 600 - self.height - 17:
                self.rect.y = 600 - self.height - 17

    def draw(self, screen): 
        if not self.dead:
            screen.blit(self.image, (self.rect.x, self.rect.y))