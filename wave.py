import pygame
from pygame import *
from enemy import *
from enemy2 import *

def spawn_wave(file_path):
	sprites = []
	file = open(file_path, 'r')
	num = int(file.readline())
	while num != 0:
		enemy_desc = file.readline().split()
		if enemy_desc[0] == 'enemy':
			enemy = Enemy(int(enemy_desc[1]), int(enemy_desc[2]))
			sprites.append(enemy)
		elif enemy_desc[0] == 'enemy2':
			enemy = Enemy2(int(enemy_desc[1]), int(enemy_desc[2]))
			sprites.append(enemy)
		num -= 1
	return(sprites)
