import pygame, os, sys
from pygame import *

_image_library = {}
def get_image(path):
		global _image_library
		if getattr(sys, 'frozen', False):
			CurrentPath = sys._MEIPASS
		else:
			CurrentPath = os.path.dirname(__file__)
		new_path = os.path.join(CurrentPath, path)
		image = _image_library.get(new_path)
		if image == None:
				canonicalized_path = new_path.replace('/', os.sep).replace('\\', os.sep)
				image = pygame.image.load(canonicalized_path).convert_alpha()
				_image_library[new_path] = image
		return image
