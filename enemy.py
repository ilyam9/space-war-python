# -*- coding: utf-8 -*-

import pygame
from pygame import *

import math, image_library
from pyganim import *

MOVE_SPEED = 2

class Enemy(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.velX = 0   
        self.velY = 0
        self.x = x 
        self.y = y
        self.health = 30
        self.image = image_library.get_image('assets/img/enemy.png')
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.image = pygame.transform.scale(self.image, (self.width * 2, self.height * 2))
        self.rect = Rect(x, y, self.width * 2, self.height * 2)
        self.dead = False
        self.hurt = False
        self.timer = 0
        self.shot = True
        self.shoot_x = 0

    def update(self):
        if not self.dead:
            self.velX = -MOVE_SPEED
            self.rect.x += self.velX
            self.rect.y = math.cos(self.rect.x / 50) * 50 + self.y
            if self.health <= 0:
                self.dead = True 
            if self.dead:
                self.kill()
            if self.hurt: 
                self.timer += 1
                self.image = image_library.get_image('assets/img/enemy_hurt.png')
                self.image = pygame.transform.scale(self.image, (self.width * 2, self.height * 2))
                if self.timer == 5:
                    self.hurt = False
                    self.timer = 0
                    self.image = image_library.get_image('assets/img/enemy.png')
                    self.image = pygame.transform.scale(self.image, (self.width * 2, self.height * 2))

    def draw(self, screen): 
        if not self.dead:
            screen.blit(self.image, (self.rect.x, self.rect.y))
