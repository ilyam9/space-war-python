import pygame, image_library
from pygame import *

def load_dialogue(file_path):
	file = open(file_path, 'r')
	image_name = file.readline()
	char_name = file.readline()
	text = [line for line in file]
	return [image_name, char_name, text]

def draw_dialogue(screen, dialogue, current_phrase):
	name_font = pygame.font.Font("assets/fonts/ARCADECLASSIC.ttf", 30)
	text_font = pygame.font.Font("assets/fonts/ARCADECLASSIC.ttf", 18)
	name = name_font.render(dialogue[1][:-1], 1, (255,255,255))
	text = text_font.render(dialogue[2][current_phrase].strip('\n'), 1, (255,255,255))
	screen.blit(image_library.get_image("assets/img/" + dialogue[0][:-1]), (0, 400))
	screen.blit(name, (40, 440))
	screen.blit(text, (40, 480))

